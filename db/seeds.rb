SecretMenuItem.create(menu_name: 'Chipotle Nachos', restaurant_name: 'Chipotle', menu_description: 'Build a plate of nachos with all of your favorite fixings')
SecretMenuItem.create(menu_name: 'Starbucks butterbeer Frappuccino', restaurant_name: 'Starbucks',
                      menu_description: 'Combine three pumps of toffee nut syrup and three pumps of caramel with a Crème Frappuccino base')
SecretMenuItem.create(menu_name: 'Skittles', restaurant_name: 'Jamba Juice', menu_description: 'A mixture of lemonade, lime sherbet, frozen yogurt, and strawberries')
SecretMenuItem.create(menu_name: "McDonald's McFlurry", restaurant_name: "McDonald's", menu_description: 'Mix your favorite candy or cookie into a McFlurry for a personalized treat')
SecretMenuItem.create(menu_name: 'In-N-Out Animal Style Fries', restaurant_name: 'In-N-Out', menu_description: 'Add special sauce, grilled onions, and melted cheese to your fries')
SecretMenuItem.create(menu_name: 'Subway Pizza Sub', restaurant_name: 'Subway', menu_description: 'Create a sandwich with marinara sauce, cheese, and your favorite pizza toppings')
SecretMenuItem.create(menu_name: 'Taco Bell Cheesarito', restaurant_name: 'Taco Bell', menu_description: 'Order a cheese quesadilla and add beef for a secret cheesy burrito')
SecretMenuItem.create(menu_name: 'Panera Bread Power Chicken Hummus Bowl', restaurant_name: 'Panera Bread',
                      menu_description: 'Enjoy a bowl with chicken, hummus, and various fresh ingredients')
SecretMenuItem.create(menu_name: 'Burger King Frings', restaurant_name: 'Burger King', menu_description: 'Get a mix of onion rings and fries for a unique side')
SecretMenuItem.create(menu_name: "Dunkin' Donuts Snickers Iced Coffee", restaurant_name: "Dunkin' Donuts",
                      menu_description: 'Add caramel, hazelnut, and chocolate syrup to your iced coffee for a Snickers-inspired drink')
SecretMenuItem.create(menu_name: 'Chick-fil-A Spicy Char', restaurant_name: 'Chick-fil-A',
                      menu_description: 'Order a spicy chicken sandwich and a char-grilled sandwich for a flavorful combo')
SecretMenuItem.create(menu_name: 'KFC Triple Down', restaurant_name: 'KFC', menu_description: 'Replace the buns of a sandwich with fried chicken for a unique take on a classic')
SecretMenuItem.create(menu_name: 'Pizza Hut Cheeseburger Pizza', restaurant_name: 'Pizza Hut', menu_description: 'Enjoy a pizza topped with ground beef, cheese, and classic burger toppings')
