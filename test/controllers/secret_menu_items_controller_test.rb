require 'test_helper'

class SecretMenuItemsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get secret_menu_items_index_url
    assert_response :success
  end

  test 'should get show' do
    get secret_menu_items_show_url
    assert_response :success
  end

  test 'should get create' do
    get secret_menu_items_create_url
    assert_response :success
  end

  test 'should get update' do
    get secret_menu_items_update_url
    assert_response :success
  end

  test 'should get destroy' do
    get secret_menu_items_destroy_url
    assert_response :success
  end
end
