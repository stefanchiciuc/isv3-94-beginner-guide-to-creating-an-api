class SecretMenuItemsController < ApplicationController
  before_action :set_secret_menu_item, only: [:show, :update, :destroy]

  def index
    @secret_menu_items = SecretMenuItem.all
    render json: @secret_menu_items
  end

  def show
    render json: @secret_menu_item
  end

  def create
    @secret_menu_item = SecretMenuItem.create(secret_menu_item_params)
    render json: @secret_menu_item
  end

  def update
    @secret_menu_item.update(secret_menu_item_params)
    render json: @secret_menu_item
  end

  def destroy
    @secret_menu_item.destroy
    render json: { message: 'Secret menu item deleted successfully' }
  end

  private

  def set_secret_menu_item
    @secret_menu_item = SecretMenuItem.find(params[:id])
  end

  def secret_menu_item_params
    params.permit(:menu_name, :restaurant_name, :menu_description)
  end
end

