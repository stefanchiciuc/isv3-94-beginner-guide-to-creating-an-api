 # Beginner's guide to creating an API from scratch using Rails

   ## Overview

   This API allows you to perform the following actions:

   - **INDEX**: Get a list of all secret menu items.
   - **SHOW**: Get details of a specific secret menu item.
   - **CREATE**: Add a new secret menu item.
   - **UPDATE**: Update an existing secret menu item.
   - **DELETE**: Remove a secret menu item from the database.

## Getting Started

Follow these steps to set up and run the Menu API:

1. Clone the repository to your local machine.
2. Run Docker Compose to start the containers.
```bash
docker-compose up -d
```
3. Create the database and run the migrations.
```bash
docker-compose exec app rails db:create
docker-compose exec app rails db:migrate
```
4. Run the seed data to populate the database.
```bash
docker-compose exec app rails db:seed
```
5. Open your browser and navigate to http://localhost:3000/secret_menu_items You should see a list of secret menu items.

6. Use a tool like Postman to test the API endpoints.
#### API Endpoints:
    * GET /secret_menu_items: Get a list of all secret menu items.
    * GET /secret_menu_items/:id: Get details of a specific secret menu item.
    * POST /secret_menu_items: Add a new secret menu item.
    * PUT /secret_menu_items/:id: Update an existing secret menu item.
    * DELETE /secret_menu_items/:id: Remove a secret menu item.
